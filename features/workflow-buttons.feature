@api @workflow-save-as-draft
Feature: Nodes with workflows have "Save as draft" and "Update workflow" buttons
  In order to manage content workflows conveniently
  As a site user
  I want "Save as draft" and "Update workflow" buttons on all workflow-enabled node forms

  Background:
    Given I am logged in as an "Administrator"

  # @TODO: create fixtures to test this.
  @wip
  Scenario Outline: Workflow buttons appear on Publication node forms.
     When I am on "node/add/<CONTENT_TYPE>"
     Then I should see the "Save as draft" button
      And I should see the "Update workflow" button
    Examples:
      | CONTENT_TYPE         |
      | external_publication |
      | informal_publication |

  Scenario: Workflow buttons do not appear on node forms without workflows.
     When I am on "node/add/page"
     Then I should see the "Save" button
     Then I should not see the "Save as draft" button
      And I should not see the "Update workflow" button
